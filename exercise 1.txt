﻿In order to develop the solution of this problem, I have used Java as POO.

We have resolved this problem in diferent ways:

1. Using TreeSet in order to avoid duplicate items and order while we are adding elements.
2. Using HashSet in order to avoid duplicate items. Then, we sort the elements with Collections utils.
3. Using ArrayList. We have to add elements if the element is not present in the list. Then, we sort with
Collections sort.

Because of the for/while, the big-0 annotation is 0(n).