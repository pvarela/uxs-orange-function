package uxs.orange.function.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractNumbersHelper {

	private static final String REGEX = "\\s*[a-zA-Z]+";

	public static List<Integer> getNumbers(String text) {

		SortedSet<Integer> sortedNumbers = new TreeSet<Integer>();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(text);

		while (m.find()) {
			sortedNumbers.add(Integer.parseInt(m.group()));
		}

		List<Integer> numbers = new ArrayList<Integer>(sortedNumbers);
		return numbers;
	}

	public static List<Integer> getNumbers2(String text) {

		Set<Integer> sortedNumbers = new HashSet<Integer>();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(text);

		while (m.find()) {
			sortedNumbers.add(Integer.parseInt(m.group()));
		}

		List<Integer> numbers = new ArrayList<Integer>(sortedNumbers);
		Collections.sort(numbers);
		return numbers;
	}

	public static List<Integer> getNumbers3(String text) {

		List<Integer> numbers = new ArrayList<Integer>();
		Map<String, Boolean> aux = new HashMap<String, Boolean>();
		String[] matches = text.split(REGEX);
		for (String s : matches) {
			if (!"".equals(s) && !aux.containsKey(s)) {
				aux.put(s, true);
				numbers.add(Integer.valueOf(s));
			}
		}

		Collections.sort(numbers);

		return numbers;
	}

}
