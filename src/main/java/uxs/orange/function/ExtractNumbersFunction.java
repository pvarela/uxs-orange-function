package uxs.orange.function;

import java.util.List;
import java.util.Scanner;

import uxs.orange.function.helpers.ExtractNumbersHelper;

/**
 * Hello world!
 *
 */
public class ExtractNumbersFunction {
	public static void main(String[] args) {
//		String texto = "A56B455VB23GTY23J";
		Scanner reader = new Scanner(System.in); // Reading from System.in
		System.out.println("Enter a string: ");
		String texto = reader.nextLine();
		reader.close();
		List<Integer> numbers = ExtractNumbersHelper.getNumbers(texto);
		System.out.println(numbers);
	}

}
