package uxs.orange.function;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import uxs.orange.function.helpers.ExtractNumbersHelper;

/**
 * Unit test for simple App.
 */
@RunWith(JUnitPlatform.class)
public class ExtractNumbersFunctionTest {

	private final String TEXT = "A56B455VB23GTY23J";

	@Test
	public void checkPerformance() {

		long startTime = 0;
		long endTime = 0;
		long duration = 0;

		startTime = System.nanoTime();
		ExtractNumbersHelper.getNumbers(TEXT);
		endTime = System.nanoTime();
		duration = (endTime - startTime);
		System.out.println("First method duration: " + duration + " ns");

		startTime = System.nanoTime();
		ExtractNumbersHelper.getNumbers2(TEXT);
		endTime = System.nanoTime();
		duration = (endTime - startTime);
		System.out.println("Second method duration: " + duration + " ns");

		startTime = System.nanoTime();
		ExtractNumbersHelper.getNumbers3(TEXT);
		endTime = System.nanoTime();
		duration = (endTime - startTime);
		System.out.println("Third method duration: " + duration + " ns");

	}

	@Test
	public void testExtractNumbers() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(23);
		numbers.add(56);
		numbers.add(455);
		assertEquals(numbers, ExtractNumbersHelper.getNumbers(TEXT));
	}

	@Test
	public void testExtractNumbers2() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(23);
		numbers.add(56);
		numbers.add(455);
		assertEquals(numbers, ExtractNumbersHelper.getNumbers2(TEXT));
	}

	@Test
	public void testExtractNumbers3() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(23);
		numbers.add(56);
		numbers.add(455);
		assertEquals(numbers, ExtractNumbersHelper.getNumbers3(TEXT));
	}

}
